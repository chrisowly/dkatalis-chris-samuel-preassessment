package Test;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

public class SpringBootProblem15 {
	
	public static void main(String[] args) {
	    //type your code here
		String firstDate = "2018-09-25T02:10:24+07:00";
		String secondDate = "2019-08-25T02:10:24+07:00";
		
		DateTimeZone timeZone = DateTimeZone.UTC;
		
		DateTime dateTimeFirstDate = new DateTime(firstDate, timeZone);
		DateTime dateTimeSecondDate = new DateTime(secondDate, timeZone);
		
		Interval intervalTime = new Interval( dateTimeFirstDate, dateTimeSecondDate );
		
		System.out.print( "Interval Time is: " + intervalTime );
	}
}
