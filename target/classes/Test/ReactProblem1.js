import React from 'react';

const MyComponent = function() {
	const helloWord = 'Welcome User to New World!';
	
	return <InitializeLayout value={helloWord} />;
}

function InitializeLayout(property) {
	
	return 
	(
			<div>
				<h1>{property.value}</h1>
			</div>
			
	);
}

export default MyComponent;