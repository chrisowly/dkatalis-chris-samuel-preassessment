const Product = props => {
  const plus = () => {
    // Call props.onVote to increase the vote count for this product
    // change code below this line
	  products.map((product, index) =>
	  	<GroceryApp product={product} key={index}
	  		onClick={GroceryApp.onVote.bind(1, product)}
	  );

    // change code above this line
  };
  const minus = () => {
    // Call props.onVote to decrease the vote count for this product
    // change code below this line
	  products.map((product, index) =>
	  	product.votes = product.votes--;
	  	<GroceryApp product={product} key={index}
	  		onClick={GroceryApp.onVote.bind(-1, index)}
	  );

    // change code above this line
  };
  return (
      <li>
        <span>{/* Product name */}</span> - <span>votes: {/* Number of votes*/}</span>
        <button onClick={plus}>+</button>{" "}
        <button onClick={minus}>-</button>
      </li>
  );
};



class GroceryApp extends React.Component {

  // Finish writing the GroceryApp class

  onVote = (dir, index) => {
    // Update the products array accordingly â€¦
    // change code below this line
	  products.map((product, index) =>
	  	product.votes = product.votes + dir;
	  );
    // change code above this line
  };

  render() {
    return (
        <ul>
          {/* Render an array of products, which should call this.onVote when + or - is clicked */}
          {/*  change code below this line */}



          {/*  change code above this line */}
        </ul>
    );
  }
}

document.body.innerHTML = "<div id='root'></div>";

ReactDOM.render(<GroceryApp
    products={[
      { name: "Oranges", votes: 0 },
      { name: "Apples", votes: 0 },
      { name: "Bananas", votes: 0 }
    ]}
/>, document.getElementById('root'))